# Project name #

This README cointains project descriptions and documents whatever steps are necessary to get your application up and running. 


[![Build Status](https://travis-ci.com/zpervan/AirQualityApp.svg?branch=master)](https://travis-ci.com/zpervan/AirQualityApp)

Add a build status badge to immediately see the status of the project.

## Project description ##

A short description about the project and what problems does it solve.

## Setup ##

### Software and hardware setup configuration ###

Summary of the setup (software and hardware) which is in the project. 

Example:

*Software:*

* Ubuntu 20.04
* Nvidia Docker for Jetson Nano
* Python, Version: 3
* gcc, Version: 10.0
* bazel Version: 4.0
* OpenCV3

*Hardware:*

* Nvidia Jetson Nano
* Basler camera

### Installation ###

Describes how to install all needed dependencies to successfully set up the project.
The preferred way is to create a script that will install all dependencies. If there are
special steps which need to be executed, then describe those steps in detail.

Example:

Run the `install_linux.sh` with `sudo` privileges to install all dependencies for this project.
Run the `install_windows.bat` with `admin` privileges to install all dependencies for this project.

To install TensorFlow on Windows, download Anaconda and do the following:

* Step 1 ...
* Step 2 ...

### Database onfiguration ###

If there are databases or AWS services that need to be connected with the repository, include a detailed description on how to connect with the repo and run them.

## Run ##

A detailed description on how to run the project. A preferred way is also to write a script and contains all necessary commands to run the project.

## Test ##

A detailed description on how to run the tests. 

## Useful links ##

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Writing READMEs by Udacity](https://github.com/udacity/ud777-writing-readmes)
* [Choose a license](https://choosealicense.com/)